import casadi as csd
import numpy as np


def forwardeuler(ode,h,t,x,u):
  """
  Apply the Forward Euler method to the given ODE.
  Parameters:
  - ode: Function handle that yields a CasADi matrix.
    Inputs:
    - t: time
    - x: state vector
    - u: input vector
    Outputs:
    - dx: derivatives of state vector x
  - h: step size
  - t: time
  - x: state vector
  - u: input vector
  """
  #convert inputs to lists if they are not iterables
  if not hasattr(x,'__iter__'):
    x = [x]
  if not hasattr(u,'__iter__'):
    u = [u]
  
  dx = ode(t,x,u)

  x_col = csd.DM(x)

  res = x_col + dx*h
  return res

def rk4(ode, h, t, x, u):
  """
  Apply the RK4 method to the given ODE.
  Parameters:
  - ode: Function handle that yields a CasADi matrix.
    Inputs:
    - t: time
    - x: state vector
    - u: input vector
    Outputs:
    - dx: derivatives of state vector x
  - h: step size
  - t: time
  - x: state vector
  - u: input vector
  """
  #convert inputs to lists if they are not iterables
  if not hasattr(x,'__iter__'):
    x = [x]
  if not hasattr(u,'__iter__'):
    u = [u]
  k1 = ode(t,x,u)
  k2 = ode(t+h/2, x+h/2*k1, u)
  k3 = ode(t+h/2, x+h/2*k2, u)
  k4 = ode(t+h,   x+h*k3, u)

  x_col = csd.DM(x)
  res = x_col + h/6*(k1 + 2*k2 + 2*k3 + k4)

  return res

# Add function names here
ode_solver_dict = {
  "forwardeuler": forwardeuler,
  "rk4": rk4
}