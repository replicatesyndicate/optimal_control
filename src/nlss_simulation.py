#!/usr/bin/env python3

## Setup, Imports and Definitions
import rospy

# Mathematics
import numpy as np 
import casadi as csd

# Plotting (if required)
from matplotlib import pyplot as plt
plt.ion() # turn on interactive mode

# Local imports
from ode_solver import *
from state_space_setup import nonlinear_ss

# ROS messages setup
from optimal_control.msg import SystemState, SystemInput
from std_msgs.msg import Empty

class PendulumCartSim:
  def __init__(self):
    ## ROS Initializations and Definitions
    #Initialize ROS node
    rospy.init_node("pendulum_cart_sim")
    rospy.on_shutdown(self.shutdown_hook)

    ## Attribute initialization
    # Timing
    self.start_time = rospy.Time.now()
    # self.wait = True

    # Load up the parameters from rospy
    self.get_params()

    # Callback holders
    self.input = np.asarray([0]*self.input_size).reshape((-1,1))



    ## ROS publisher/subscriber definitions
    # Publisher
    self.state_pub = rospy.Publisher("/pendcart/state", SystemState, queue_size=1)
    # Subscriber
    self.input_sub = rospy.Subscriber("/pendcart/input", SystemInput, self.input_cb)
    # self.wait_sub = rospy.Subscriber("/pendcart/wait", Empty, self.wait_cb)

    # Plotting attributes
    self.time_array = []
    self.state_array = None


  ## Parameter access  
  def get_params(self):
    #Initial system conditions
    if rospy.has_param("initial_conditions"):
      state = rospy.get_param("initial_conditions")
    else: #defaults
      state = [0, 0.25, np.pi/12, 0] #pendcart
    # column vector
    self.state = np.asarray(state).reshape((-1,1))
      
    #ROS timing parameters - set rate of execution
    if rospy.has_param("sim_freq"):
      self.sim_freq = rospy.get_param("sim_freq")
      self.stepsize = 1/self.sim_freq
      self.rate = rospy.Rate(self.sim_freq)
    else: #defaults
      self.sim_freq = 100
      self.stepsize = 1/self.sim_freq
      self.rate = rospy.Rate(self.sim_freq)

    if rospy.has_param("solver"):
      self.solver = ode_solver_dict[rospy.get_param("solver")]
    else:
      self.solver = ode_solver_dict["rk4"]
    
    if rospy.has_param("input_size"):
      self.input_size = rospy.get_param("input_size")
    else:
      self.input_size = 1

  ## Callbacks
  def input_cb(self,msg):
    # column vector
    self.input = np.asarray(msg).reshape((-1,1))
  
  # def wait_cb(self,msg):
  #   #when triggered, make wait flag false
  #   self.wait = False
     
  ## Topic publisher functions
  def state_topic(self):
    """
    Publish the robot state.
    """
    msg = SystemState()
    msg.header.stamp = rospy.Time.now()
    msg.state = np.array(self.state)
    self.state_pub.publish(msg)

    #plt part
    self.time_array.append((msg.header.stamp-self.start_time).to_sec())
    if self.state_array is None:
      self.state_array = msg.state
    else:
      self.state_array = np.hstack((self.state_array,msg.state))


  ## Functions
  def simulate(self):
    """
    Main function in script
    """

    # get current time in genpy.rostime.Duration format
    # if float format is needed, use .to_sec() or .to_nsec()
    self.current_time = rospy.Time.now() - self.start_time

    # update state
    self.state = self.solver(
      nonlinear_ss,
      self.stepsize,
      self.current_time.to_sec(),
      self.state,
      self.input      
      )


  ## Shutdown hook
  def shutdown_hook(self):
    """
    Execute on shutdown
    """
    # Extract plot
    # plt.plot(self.time_array,self.state_array.T)
    # plt.legend(["x0","x1","x2","x3"])
    # plt.xlabel("t")
    # plt.show()
    rospy.loginfo("Shutting down in 10 seconds. Please save your plot.")
    rospy.sleep(rospy.Duration(10))


def start():
  """
  Main code block to execute when called from the command-line
  Initialize rospy and the class instances
  Run the subscribers and publishers
  """
  ## ROS Initializations and Definitions

  # Initialize module
  pcs = PendulumCartSim()

  # Publish initial state
  pcs.state_topic()

  ## Run main loop
  while not rospy.is_shutdown():
    # if pcs.wait is False:
    # Simulate robot states
    pcs.simulate()
    # Publish current state
    pcs.state_topic()
    
    ## Keep the loop running at a specified rate
    pcs.rate.sleep()  
    

##Main code block
if __name__ == "__main__":
  start()