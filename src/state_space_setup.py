#!/usr/bin/env python3
import numpy as np
import casadi as csd

# inverted pendulum-cart system example
# def nonlinear_ss(t,x,u):
#   '''
#   Define nonlinear state space equations in the CasADi framework.
#   Parameters:
#   - t: current time
#   - x: state vector
#   - u: input vector
#   '''
#   #convert inputs to lists if they are not iterables
#   if not hasattr(x,'__iter__'):
#     x = [x]
#   if not hasattr(u,'__iter__'):
#     u = [u]
#   #initialize state vector size
#   dx = [0]*len(x)

#   #define denominators here if they are too long
#   den_1 = 0.075*csd.cos(x[2])**2+0.7
#   den_3 = 0.02*csd.cos(x[2])-0.1867/csd.cos(x[2])

#   #derivative terms
#   dx[0] = x[1]
#   dx[1] = (u[0] - 0.12*x[1] - 0.02*csd.sin(x[2])*(x[3]**2) - 0.7357*csd.cos(x[2])*csd.sin(x[2]))/den_1
#   dx[2] = x[3]
#   dx[3] = (-u[0]+0.12*x[1]-6.867*csd.tan(x[2])+0.02*csd.sin(x[2])*x[3]**2)/den_3

#   #return as a CasADi matrix
#   return csd.DM(dx)

# found example to test scripts
def nonlinear_ss(t,x,u):
  '''
  Define nonlinear state space equations in the CasADi framework.
  Parameters:
  - t: current time 
  - x: state vector
  - u: input vector
  '''
  dx = [0]*4

  # try:
  #derivative terms
  dx[0] = csd.tan(x[3])*x[1] + 2*u[1]-1
  dx[1] = x[0] - csd.sin(x[1])
  dx[2] = 13*x[3] + u[0] + 1
  dx[3] = 0.2*x[0]
  # except IndexError:
  #   pass

  #return as a CasADi matrix
  return csd.DM(dx)

# inverted pendulum-cart example
def linear_ss(t,x,u):
  """
  Define linear state space equations in the CasADi framework.
  Parameters:
  - t: current time
  - x: state vector
  - u: input vector
  """
  # convert inputs to lists if they are not iterables
  if not hasattr(x,'__iter__'):
    x = [x]
  if not hasattr(u,'__iter__'):
    u = [u]

  # Allocate numpy arrays
  A = np.zeros((4,4))
  B = np.zeros((4,1))

  # Define system matrix and input matrix. 
  # Make sure to use proper dimensions
  A[0] = [0, 1, 0, 0]
  A[1] = [0, -0.1548, -0.9494, 0]
  A[2] = [0, 0, 0, 1]
  A[3] = [0, -0.72, 41.202, 0]

  B[0] = [0]
  B[1] = [1.290]
  B[2] = [0]
  B[3] = [6]

  #State-space equation here
  x_vec = np.asarray(x).reshape((-1,1))
  u_vec = np.asarray(u).reshape((-1,1))
  # dx = Ax + Bu
  dx = np.dot(A,x_vec) + np.dot(B,u_vec)

  # results as a CasADi matrix
  return csd.DM(dx)
  
  